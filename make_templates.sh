#!/bin/bash

INPUT=""
OUTPUT=""
DESTDIR=""

while getopts ":i:o:d:" opt; do
case $opt in
i)
INPUT=$OPTARG
;;
o)
OUTPUT=$OPTARG
;;
d)
DESTDIR=$OPTARG
;;
\?)
echo "Invalid option: -$OPTARG" >&2
exit 1
;;
:)
echo "Option -$OPTARG requires an argument." >&2
exit 1
;;
esac
done

# сюда пишем все шаблоны
TEMPLATES="$INPUT/templates/master.tmpl"
TEMPLATES="$TEMPLATES $INPUT/templates/news.tmpl"

# прожевываем шаблоны в срр-шник
cppcms_tmpl_cc $TEMPLATES -o $INPUT/all_tmpl.cpp

if [ ! -d $DESTDIR ]; then
mkdir $DESTDIR
fi
cd $DESTDIR

# собираем шаблоны в библиотеку
g++ -shared -fPIC $INPUT/all_tmpl.cpp -o ./libcpp_defskin.so -lcppcms -lbooster

cp -R $INPUT/media .
