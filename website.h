#ifndef WEBSITE_H
#define WEBSITE_H

#include <cppcms/applications_pool.h>
#include <cppcms/url_dispatcher.h>
#include <cppcms/http_response.h>
#include <cppcms/application.h>
#include <cppcms/url_mapper.h>
#include <cppcms/service.h>

#include "data/tmpl_master.h"
#include "data/tmpl_news.h"

//-------------------------------------------------------------------------------------
// Dsc: Наш класс отрисовки страниц, при запросе некоторого адреса пользователем
//      В первую очередь он попадет сюда
//-------------------------------------------------------------------------------------
class WebSite : public cppcms::application
{
public:
	WebSite(cppcms::service &s);
	//-------------------------------------------------------------------------------------
	// Dsc: Функция в которую мы попадем, если иного не указано в конструкторе
	//      ( об этом позже )
	//-------------------------------------------------------------------------------------
	virtual void main(std::string path);
	virtual void master(std::string path);
	virtual void news(std::string path);
};


#endif // WEBSITE_H
