#include "website.h"

WebSite::WebSite(cppcms::service &s)
	: cppcms::application(s)
{
	dispatcher().assign("/news(.*)",&WebSite::news,this,1);
	mapper().assign("news","/news");

	dispatcher().assign("(/?)",&WebSite::master,this,1);
	mapper().assign("master","/");
}

void WebSite::main(std::string path)
{
	cppcms::application::main(path);
}

void WebSite::master(std::string path)
{
	Data::Master tmpl;
	tmpl.page.title = "Главная";
	tmpl.page.description = "description";
	tmpl.page.keywords = "keywords";
	tmpl.page.menuList.insert(std::pair<std::string,std::string>("/","MASTER"));
	tmpl.page.menuList.insert(std::pair<std::string,std::string>("/news","NEWS"));
	render("Master",tmpl);
}
//-------------------------------------------------------------------------------------
// Dsc: Рендеринг новостей
//-------------------------------------------------------------------------------------
void WebSite::news(std::string path)
{
	Data::News tmpl;
	tmpl.page.title = "Новости";
	tmpl.page.description = "description";
	tmpl.page.keywords = "keywords";
	tmpl.page.menuList.insert(std::pair<std::string,std::string>("/","MASTER"));
	tmpl.page.menuList.insert(std::pair<std::string,std::string>("/news","NEWS"));
	tmpl.mainNews = "Сенсация! У нас на сайте ничего не произошло!";
	render("News",tmpl);
}
