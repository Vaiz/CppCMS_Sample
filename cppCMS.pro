#-------------------------------------------------
#
# Project created by QtCreator 2015-07-12T20:02:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cppCMS
TEMPLATE = app
DESTDIR = ../../Build/cppCMS

SOURCES += main.cpp\
        mainwindow.cpp \
    website.cpp \
    data/tmpl_master.cpp

HEADERS  += mainwindow.h \
    website.h \
    data/tmpl_master.h \
    data/tmpl_news.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY =

LIBS += -L/usr/local/lib/ -lbooster -lcppcms
INCLUDEPATH += /usr/local/include
DEPENDPATH += /usr/local/include

DISTFILES += \
    make_templates.sh
