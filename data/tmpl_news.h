#ifndef TMPL_NEWS_H
#define TMPL_NEWS_H

#include "tmpl_master.h"

namespace Data
{
// Dsc: Новостной контент
struct News :public Master
{
	std::string mainNews; // Главная новость

	News() : Master(){}
	// Dsc: Ленивый деструктор
	~News(){}
};
}


#endif // TMPL_NEWS_H
