#ifndef TMPL_MASTER_H
#define TMPL_MASTER_H

#include <cppcms/view.h>

namespace Data
{
// Dsc: Структура основной информации о странице
struct infoPage
{
	std::string title;                         // титул страницы
	std::string description;                   // описание страницы
	std::string keywords;                      // ключевые слова страницы
	std::map<std::string,std::string> menuList;   // список выводимых пунктов меню (url,desc)

	infoPage();
	~infoPage();
};

// Dsc: Базовый контент который есть на каждой странице
struct Master :public cppcms::base_content
{
	infoPage    page;

	Master();
	// Ленивый деструктор, без него библиотека не грузится о_О
	~Master(){}
};
}
#endif // TMPL_MASTER_H
