#include "mainwindow.h"
#include <QApplication>

#include "website.h"

int main(int argc, char *argv[])
{
	/*QApplication a(argc, argv);
	MainWindow w;
	w.show();

	return a.exec();*/

	try
	{
		char **cppCmsArgs;
		cppCmsArgs = new char*[3];
		cppCmsArgs[0] = new char[256];
		strcat(cppCmsArgs[0], argv[0]);
		cppCmsArgs[1] = new char[3];
		strcat(cppCmsArgs[1], "-c\0");
		cppCmsArgs[2] = new char[50];
		strcat(cppCmsArgs[2], "media/config.json\0");
		// создаем сервис
		cppcms::service srv(3, cppCmsArgs);
		// задаем корень
		srv.applications_pool().mount(cppcms::applications_factory<WebSite>());
		// запускаем
		srv.run();
	}
	catch(std::exception const &e)
	{
		std::cerr << "Failed: " << e.what() << std::endl;
		std::cerr << booster::trace(e) << std::endl;
		return 1;
	}
	return 0;
}
